package CIdemo;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

public class TestCalc {

	@Test
	public void testadd() {
		int addSum = new Calc().add(4, 3);
		assertEquals(7, addSum);
	}
	
	@Test
	public void testsubtract() {
		int subtractSum = new Calc().subtract(4, 3);
		assertEquals(1, subtractSum);
	}

}
